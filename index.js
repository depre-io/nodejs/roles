(function () {
  'use strict';
  let Roles = {
    "owner": "owner",
    "modifier": "modifier",
    "service": "service",
    "administrator": "administrator",
    "designer": "designer",
    "reviewer": "reviewer",
    "editor": "editor",
    "publisher": "publisher",
    "viewer": "viewer",
    "preparer": "publisher",
    "accessor": "accessor",
    "auditor": "auditor",
    "archiver": "archiver"
  };
  module.exports = Roles;
}());
